
/**
 * Module dependencies.
 */

var express = require('express');
var mongoose = require('mongoose');

var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));


app.get('/', function(req, res){
  res.sendfile('./public/index.html');
});

app.get("/jsoneditor", function(req, res){
  res.sendfile('./public/jsoneditor.html');
});

//listar
app.get('/listar', function(req, res){
   Regla.find({}, function(error, reglas){
      if(error){
         res.send('Error.');
      }else{
         res.send(reglas);
      }
   })
});

//editar
app.get('/recuperar', function(req, res){
   Regla.findById(req.query._id, function(error, documento){
      if(error){
         res.send('Error.');
      }else{
         res.send(documento);
      }
   });
});

//guardar
app.post('/guardar', function(req, res){
   if(req.query._id == null){
      //Inserta
      var regla = new Regla({
         nombre: req.query.nombre,
         condicion: req.query.condicion

      });
      regla.save(function(error, documento){
         if(error){
            res.send('Error.');
         }else{
            res.send(documento);
         }
      });
   }else{
      //Modifica
      Regla.findById(req.query._id, function(error, documento){
         if(error){
            res.send('Error al intentar modificar la regla.');
         }else{
            var regla = documento;
            regla.nombre = req.query.nombre,
            regla.condicion = req.query.condicion
            regla.save(function(error, documento){
               if(error){
                  res.send('Error.');
               }else{ 
                  res.send(documento);
               }
            });
         }
      });
   }
});

//eliminar
app.post('/eliminar', function(req, res){
   Regla.remove({_id: req.query._id}, function(error){
      if(error){
         res.send('Error.');
      }else{
         res.send('Ok');
      }
   });
});


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}


//Documentos
var ReglaSchemaJSON = mongoose.Schema({
   nombre: String,
   condicion: String
});

var Regla = mongoose.model('Regla', ReglaSchemaJSON);


//conexion base datos
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/crudangular', function(error){
   if(error){
      throw error; 
   }else{
      useMongoClient: true, 
      console.log('Conectado a MongoDB');
   }
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

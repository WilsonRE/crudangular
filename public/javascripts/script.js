var aplicacion = angular.module('aplicacion', []);
aplicacion.controller('Reglas', function($scope, $http) {
   $scope._id = null;
   $scope.nombre = '';
   $scope.condicion = '';
   $scope.reglas = [];
   $scope.cargarReglas = function(){
      $http({
         method: 'GET', url: '/listar'
      }).
      success(function(data) {
         if(typeof(data) == 'object'){
            $scope.reglas = data;
         }else{
            alert('Error al intentar recuperar las reglas.');
         }
      }).
      error(function() {
         alert('Error al intentar recuperar las reglas.');
      });
   };
   $scope.guardarReglas = function() {
      $http({
         method: 'POST',
         url: '/guardar',
         params: {
            nombre: $scope.nombre,
            condicion: $scope.condicion,
            _id: $scope._id
         }
      }).
      success(function(data) {
         if(typeof(data) == 'object'){
            $scope.limpiarReglas();
            $scope.cargarReglas(); 
         }else{
            alert('Error al intentar guardar la regla.');
         }
      }).
      error(function() {
         alert('Error al intentar guardar la regla.');
      });
   };
   $scope.recuperarReglas = function(indice) {
      $http({
         method: 'GET',
         url: '/recuperar',
         params: {
            _id: indice
         }
      }).
      success(function(data) {
         if(typeof(data) == 'object'){
            $scope._id = data._id;
            $scope.nombre = data.nombre;
            $scope.condicion = data.condicion;
         }else{
            alert('Error al intentar recuperar la regla.');
         } 
      }).
      error(function() {
         alert('Error al intentar recuperar la regla.');
      });
   };
   $scope.eliminarReglas = function(indice) {
      $http({
         method: 'POST',
         url: '/eliminar',
         params: {
            _id: indice
         }
      }).
      success(function(data) {
         if(data == 'Ok'){
            $scope.limpiarReglas();
            $scope.cargarReglas();
         }else{
            alert('Error al intentar eliminar la regla.');
         } 
      }).
      error(function() {
         alert('Error al intentar eliminar la regla.');
      });
   };
   $scope.limpiarReglas = function() {
      $scope._id = null;
      $scope.nombre = '';
      $scope.condicion = '';
   };
});